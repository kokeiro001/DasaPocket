﻿using System;
using ConfigR;
using PocketSharp;
using PocketSharp.Models;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace DasaPocket
{
    class Program
    {
        static void Main(string[] args)
        {
            Config.Global.LoadScriptFile("Config.csx");
            PocketClient client = new PocketClient(
                Config.Global.Get<string>("ConsumerKey"),
                Config.Global.Get<string>("AccessToken")
            );

            DateTime now = DateTime.Now;
            DateTime yesterday = new DateTime(now.Year, now.Month, now.Day);

            var articles = client.Get(
                since: yesterday,
                sort: Sort.oldest).Result;

            StringBuilder sb = new StringBuilder();
            foreach(var article in articles)
            {
                sb.AppendLine(article.Title);
                sb.AppendLine(article.Uri.ToString());
                sb.AppendLine();
            }

            string outputPath = Config.Global.Get<string>("OutputPath");
            using(TextWriter fs = new StreamWriter(outputPath, false))
            {
                fs.Write(sb.ToString());
            }
        }
    }
}
